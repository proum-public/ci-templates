# Shared pipeline templates

This repository contains pipeline templates for Gitlab CI that can be used by
different teams across different projects.
This helps to maintain a clean code base,
improves code readability and avoids code duplication.

This is based on Gitlab's support for templates which is described
[here](https://docs.gitlab.com/ee/ci/yaml/README.html#includefile).

## Usage of pipeline templates

A template can be used in the `.gitlab-ci.yml` like this:

```yaml
include:
  - project: 'proum-public/ci-templates'
    ref: master
    file: '/docker/cve_scan.yml'
```

> The template itself has a stage defined in which it will be invoked.
So one needs to make sure that the stage defined in the template also
exists in the `.gitlab-ci.yml`
