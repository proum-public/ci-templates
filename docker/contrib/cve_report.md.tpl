{{- printf "|Library|Vulnerability ID|Severity|Installed Version|Fixed Version|Title|\n" }}
{{- printf "|-------|----------------|--------|-----------------|-------------|-----|\n" }}
{{- $pkg_name := "" }}
{{- $severity := "" }}
{{- $clean := true }}
{{- range . }}
    {{- $target := .Target }}
    {{- range .Vulnerabilities }}
        {{- if eq .Severity "UNKNOWN" -}}
            {{- $severity = "Unknown" }}
        {{- else if eq .Severity "LOW" -}}
            {{- $severity = "Low" }}
        {{- else if eq .Severity "MEDIUM" -}}
            {{- $severity = "Medium" }}
        {{- else if eq .Severity "HIGH" -}}
            {{- $severity = "High" }}
        {{- else if eq .Severity "CRITICAL" -}}
            {{- $severity = "Critical" }}
        {{-  else -}}
            {{- $severity = .Severity }}
        {{- end }}
        {{- if eq $pkg_name .PkgName }}
            {{- printf "||%s|%s|%s|%s|%s|\n" .VulnerabilityID $severity .InstalledVersion .FixedVersion .Title }}
        {{- else }}
            {{- printf "|%s|%s|%s|%s|%s|%s|\n" .PkgName .VulnerabilityID $severity .InstalledVersion .FixedVersion .Title }}
            {{- $pkg_name = .PkgName }}
        {{- end }}
        {{- $clean = false }}
    {{- end }}
{{- end }}
{{- if $clean }}
    {{- printf "\n* **No vulnerabilities have been found!**" }}
{{- end }}
{{- printf "\n\n**CreatedAt**: %s" now | date "DD-MM-YYYY" }}
